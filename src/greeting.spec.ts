import 'jest';
import { GreetingMessage } from './greeting';

describe('Greeting', () => {
  it("returns 'Hello World!'", () => {
    const expected = 'Hello World!';
    const actual = GreetingMessage();

    expect(actual).toBe(expected);
  });
});
